# cv

This repo contains the CV of one Sami Paavilainen

<https://elflawerino.gitlab.io/cv/>

## Development

### Requirements

Install Hugo (<https://gohugo.io>)

Make sure to checkout the submodules (for the DevResume theme)

### Update and preview

1. Update content in config.toml
2. Run local server with `hugo server`

### Get a PDF

1. Print to PDF using an appropriate (f.ex. A4x3) paper size
